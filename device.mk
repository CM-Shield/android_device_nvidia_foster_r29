#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET_TEGRA_CAMERA ?= none
TARGET_TEGRA_GPU    ?= nvgpu-r29
TARGET_TEGRA_KERNEL ?= 310
TARGET_TEGRA_KEYSTORE ?= nvkeystore-t124
TARGET_TEGRA_OMX    ?= nvmm-r29

$(call inherit-product, device/nvidia/foster/device.mk)
include device/nvidia/icera/icera.mk
include device/nvidia/touch/raydium.mk
include device/nvidia/touch/shtouch.mk

$(call inherit-product, device/nvidia/foster_r29/vendor/foster_r29-vendor.mk)

# Init related
PRODUCT_PACKAGES += \
    rel29.rc

# Fingerprint override
BUILD_FINGERPRINT := NVIDIA/foster_e/foster:9/PPR1.180610.011/4199437_1739.5219:user/release-keys

ifeq ($(TARGET_TEGRA_KEYSTORE),nvkeystore-t124)
$(call inherit-product, device/nvidia/t124-common/vendor/keystore/keystore.mk)
endif

ifeq ($(TARGET_TEGRA_GPU),nvgpu-t124)
$(call inherit-product, device/nvidia/t124-common/vendor/nvgpu/nvgpu.mk)
endif

ifeq ($(TARGET_TEGRA_OMX),nvmm-t124)
$(call inherit-product, device/nvidia/t124-common/vendor/nvmm/nvmm.mk)
endif

# HIDL
PRODUCT_ENFORCE_VINTF_MANIFEST_OVERRIDE := true
